import 'package:flutter/material.dart';

class BuyButtom extends StatelessWidget {
  final String? text;
  final Function()? onPressed;

  const BuyButtom({Key? key, this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth:150,
      onPressed: () {},
      child: Text(
        text!, style: TextStyle(color: Colors.pink),
        textAlign: TextAlign.center,
      ),
    );
  }
}
