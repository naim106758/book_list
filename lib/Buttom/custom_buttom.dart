import 'package:flutter/material.dart';

class CustomButtom extends StatelessWidget {
  final String? text;
  final Function()? onPressed;
  const CustomButtom({Key? key, this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: 20,
      minWidth: 300,
      highlightColor: Colors.cyanAccent,
      onPressed: onPressed,
      color: Colors.indigoAccent,
      shape: StadiumBorder(),
      child: Padding(
        padding: EdgeInsets.only(top: 10, bottom: 10, right: 15, left: 15),
        child: Text(
          text!,
          style: TextStyle(color: Colors.white, fontSize: 17),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
