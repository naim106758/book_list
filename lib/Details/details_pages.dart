import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../Pages/wishlist_pages.dart';
import '../route/route_name.dart';

class DetailsPages extends StatefulWidget {
  const DetailsPages({Key? key, required this.ptoductDetails})
      : super(key: key);
  final Map ptoductDetails;

  @override
  _DetailsPagesState createState() => _DetailsPagesState();
}

class _DetailsPagesState extends State<DetailsPages> {
  late TextEditingController quantityController;

  int quantity = 1;
  bool isSelected = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    quantityController = TextEditingController(text: quantity.toString());

    return Scaffold(
        body: SafeArea(
      child: Container(
        constraints: const BoxConstraints.expand(),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                BackButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                IconButton(
                    onPressed: () {
                      addToWishlist();
                      Alert(
                        context: context,
                        type: AlertType.success,
                        title: 'Succesfull added to wishlist',
                      )
                          .show();
                      isSelected = true;
                      setState(() {});
                    },
                    icon: Icon(
                        isSelected ? Icons.favorite : Icons.favorite_border))
              ],
            ),
            Container(
             height:200,
             width: 500,
             color: Colors.white70,
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.only(
                    left: 15, right: 15, top: 10, bottom: 10),
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        Colors.deepOrange,
                        Colors.grey,
                        Colors.blue,
                        Colors.brown,
                        Colors.brown,
                      ]),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(35),
                      topRight: Radius.circular(25)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Book Name:${widget.ptoductDetails["book Name"]}"),
                    Text("Author Name:${widget.ptoductDetails["author Name"]}"),
                    Text("Department:${widget.ptoductDetails["department"]}"),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         Text("Semester:${widget.ptoductDetails["semester"]}"),
                         Text(
                           'ট:${int.parse(widget.ptoductDetails["price"]) * quantity}',
                           style: const TextStyle(fontSize: 20),
                         ),
                       ],
                     ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        const Text(
                          'Quantity:',
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                        MaterialButton(
                          color: Colors.cyan,
                          shape: const StadiumBorder(),
                          onPressed: () {
                            if (quantity > 1) quantity--;
                            setState(() {});
                          },
                          child: const Text(
                            '-',
                            style: TextStyle(color: Colors.black, fontSize: 25),
                          ),
                        ),
                        Container(
                          color: Colors.white70,
                          width:40,
                          child: TextField(
                             controller: quantityController,
                            textAlign: TextAlign.center,
                            onChanged: (value) {
                              quantity = int.parse(value);
                              setState(() {});
                            },
                            decoration: const InputDecoration(
                                 isDense: true, border: OutlineInputBorder(

                            )),
                          ),
                        ),
                        MaterialButton(
                          shape: const StadiumBorder(),
                          color: Colors.cyan,
                          onPressed: () {
                            quantity++;
                            setState(() {});
                          },
                          child: const Text(
                            "+",
                            style: TextStyle(color: Colors.black, fontSize: 18),
                          ),
                        )
                      ],
                    ),
                    const Divider(
                      thickness: 4.0,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        MaterialButton(
                          onPressed: () {
                            addToCard();
                            Alert(
                              context: context,
                              type: AlertType.success,
                              title: 'Succesfull added to cart',
                            )
                                .show();
                          },
                          shape: const StadiumBorder(),
                          color: Colors.indigo,
                          child: const Text(
                            'Cart',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        MaterialButton(
                          onPressed: () {
                            addToCard();
                            Navigator.pushNamed(context, RouteName.PeyMentPages);
                          },
                          shape: const StadiumBorder(),
                          color: Colors.indigo,
                          child: const Text(
                            'Buy now',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }

void addToCard() {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  firestore.collection('user_cart').add({
    'book Name': widget.ptoductDetails['book Name'],
    'author Name': widget.ptoductDetails['author Name'],
    'department': widget.ptoductDetails['department'],
    'semester': widget.ptoductDetails['semester'],
    'price': widget.ptoductDetails['price'],
    'quantity': quantityController.text
  });
}

void addToWishlist() {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  firestore.collection('wish_list').add({
    'book Name': widget.ptoductDetails['book Name'],
    'author Name': widget.ptoductDetails['author Name'],
    'department': widget.ptoductDetails['department'],
    'semester': widget.ptoductDetails['semester'],
    'price': widget.ptoductDetails['price'],
  });
}

}
