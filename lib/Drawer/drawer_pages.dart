
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../route/route_name.dart';

class DrawerPages extends StatefulWidget {
  const DrawerPages({Key? key}) : super(key: key);

  @override
  State<DrawerPages> createState() => _DrawerPagesState();
}

class _DrawerPagesState extends State<DrawerPages> {


  @override
  void initState() {
    super.initState();

  }

  @override

  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            title: const Text(' Delete Account'),
            leading: Icon(Icons.delete),
            onTap: (){},

          ),
          ListTile(
            title: const Text('LogOut'),
            leading: Icon(Icons.logout_rounded),
            onTap: (){
              Navigator.pushNamed(context, RouteName.LoginPages);
            },

          ),
          ListTile(
            title: Text('Home'),
            leading: Icon(Icons.home),
            onTap: () {
              Navigator.pushNamed(context, RouteName.StorePages);
            },
          ),
          ListTile(
            title: Text('Oder Pages'),
            leading: Icon(Icons.home),
            onTap: () {
              Navigator.pushNamed(context, RouteName.StorePages);
            },
          ),
        ],
      ),
    );
  }
}
