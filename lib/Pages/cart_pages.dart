import 'package:book_list/Pages/store_pages.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../Details/details_pages.dart';
import '../route/route_name.dart';
class CartPages extends StatefulWidget {
  const CartPages({ Key? key }) : super(key: key);

  @override
  _CartPagesState createState() => _CartPagesState();
}

class _CartPagesState extends State<CartPages> {
  List items=[];

  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCartData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        appBar: AppBar(
          backgroundColor: Colors.blue.shade400,
          leading: BackButton( onPressed: (){
 Navigator.pushNamed(context, RouteName.StorePages);
           },),
          title:Text('Book List',style: const TextStyle(fontSize: 18),),
        ),
        body:SafeArea(
          child: Container(
              decoration: BoxDecoration(
                gradient: const LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [

                      Colors.deepOrange,
                      Colors.grey,
                      Colors.blue,
                      Colors.brown,
                      Colors.grey,
                    ]),
                color: Colors.lightBlue,
                shape: BoxShape.rectangle,
                border: Border.all(width:2,color: Colors.white),
                borderRadius:const BorderRadius.only(topLeft: Radius.circular(35),bottomRight: Radius.circular(40)),
                boxShadow:[
                  BoxShadow(
                      color: Colors.blueGrey.shade300,
                      blurRadius:4.0,
                      spreadRadius:10,
                      offset: Offset(2,2)
                  )
                ],
              ),
              padding:const EdgeInsets.only(left:6,right:6),
              child:items.isEmpty?const Center(child: CircularProgressIndicator(
                color: Colors.white,
              ),):
              Column(
                children: [
                  const SizedBox(height: 10,),
                  Expanded(

                    child: items.isEmpty
                        ? Center(
                      child: CircularProgressIndicator(),
                    )
                        :
                    ListView.separated(
                        itemBuilder: (context, index) {
                          return Container(
                            height: 150,
                            decoration: BoxDecoration(
                                color: Colors.greenAccent.shade200,
                                borderRadius: BorderRadius.circular(20)),
                            child: Row(
                              children: [
                                Expanded(

                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      left: 15, right: 15,),
                                    child: Column(

                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Book Name:${items[index]['book Name']}",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              overflow: TextOverflow.ellipsis),
                                        ),
                                        Text(
                                          "Author Name:${items[index]['author Name']}",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              overflow: TextOverflow.ellipsis),
                                        ),
                                        Text(
                                          "Department:${items[index]['department']}",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.normal,
                                              overflow: TextOverflow.ellipsis),
                                        ),
                                        Text(
                                          "Semester:${items[index]['semester']}",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.normal),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "৳:${items[index]['price']}",
                                              style: TextStyle(fontSize: 20),
                                            ),
                                            MaterialButton(
                                              onPressed: () {
                                                removeItemFromCartData(index);
                                              },
                                              shape: StadiumBorder(),
                                              color: Colors.blueAccent,
                                              child: Text("Remove"),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                        separatorBuilder: (context, index) {
                          return SizedBox(
                            height: 10,
                          );
                        },
                        itemCount: items.length),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text('${items.length} item(s)',style: const TextStyle(fontSize: 18),),
                        MaterialButton(
                          onPressed: ( ){
                            Navigator.pushNamed(context, RouteName.PeyMentPages);
                           },
                          color: Colors.blueAccent,
                          child: Text("Buy now"),

                        )
                      ],
                    ),
                  )

                ],
              )
          ),
        )

    );
  }
  getCartData()async{
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    var cartData=await firestore.collection('user_cart').get();
    for (var doc in cartData.docs) {
      setState(() {
        Map map = {
          "book Name": doc["book Name"],
          "author Name": doc["author Name"],
          "department": doc["department"],
          "semester": doc["semester"],
          "price": doc["price"],
          'id':doc.id,
        };
        items.add(map);
      });
    }
  }
  void removeItemFromCartData(int index) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    await firestore.collection('user_cart').doc(items[index]['id']).delete();
    items.clear();
    getCartData();
    setState(() {});
  }
}
