import 'package:book_list/Details/details_pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../Drawer/drawer_pages.dart';

class HomePages extends StatefulWidget {
  const HomePages({Key? key}) : super(key: key);

  @override
  State<HomePages> createState() => _HomePagesState();
}

class _HomePagesState extends State<HomePages> {
  List catagory = [
    'All',
    'Computer',
    'Civil',
    'Electrical',
    'Electronicl',
  ];
  List catagory2 = [
    'all',
    'computer',
    'civil',
    'electrical',
    'electronicl',
  ];
  int selectdindex = 0;
  List items = [];
  var collectionName = "all";
  TextEditingController searchController=TextEditingController();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData("all");
  }
  // Icon cusIcon = Icon(Icons.search);
  // Widget cusSearchBar = Text("Book List");

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Book List",
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
        // centerTitle: true,
        // actions: [IconButton(onPressed: () {
        //   setState(() {
        //     if(
        //     this.cusIcon.icon == Icons.search
        //     ){
        //       this.cusIcon=Icon(Icons.cancel);
        //       this.cusSearchBar=TextField(
        //         textInputAction: TextInputAction.go,
        //         decoration: InputDecoration(
        //           border:InputBorder.none
        //         ),
        //         style: TextStyle(
        //           color: Colors.white,fontSize: 15
        //         ),
        //       );
        //     }else{
        //       this.cusIcon=Icon(Icons.search);
        //       this.cusSearchBar=Text("Book List");
        //     }
        //   });
        // }, icon: Icon(Icons.search))],

      ),
      drawer: DrawerPages(),
      body: Container(
          padding: EdgeInsets.only(left: 15, right: 15, top: 24, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextField(
                controller: searchController,
                decoration: InputDecoration(
                  hintText: 'Search here',hintStyle:const TextStyle(color: Colors.black87,fontWeight: FontWeight.normal),
                  suffixIcon:IconButton(onPressed: (){
                    items.clear();
                    getDataBySearch(collectionName);
                  }, icon: const Icon(Icons.search)),
                  isDense: true,
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                    BorderSide(color: Colors.grey.shade300, width:1.0),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                    BorderSide(color: Colors.grey.shade300, width:1.0),
                    borderRadius: BorderRadius.circular(16),
                  ),
                  border: OutlineInputBorder(
                    borderSide:
                    BorderSide(color: Colors.grey.shade300, width: 0.5),
                    borderRadius: BorderRadius.circular(16),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                height: 30,
                child: ListView.separated(
                    itemBuilder: (context, index) {
                      return MaterialButton(
                        onPressed: () {
                          selectdindex = index;
                          collectionName = catagory2[selectdindex];
                          items.clear();
                          getData(catagory2[selectdindex]);
                          setState(() {});
                        },
                        minWidth: 100,
                        color:
                            selectdindex == index ? Colors.indigoAccent : null,
                        child: Text(
                          catagory[index],
                          style: TextStyle(
                              color: selectdindex == index
                                  ? Colors.white
                                  : Colors.indigoAccent),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return SizedBox(
                        width: 10,
                      );
                    },
                    scrollDirection: Axis.horizontal,
                    itemCount: catagory.length),
              ),
              Expanded(

                child: items.isEmpty
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    :
                ListView.separated(
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailsPages(ptoductDetails: items[index])));
                            },
                            child: Container(
                              height: 150,
                              decoration: BoxDecoration(
                                  color: Colors.greenAccent.shade200,
                                  borderRadius: BorderRadius.circular(20)),
                              child: Row(
                                children: [
                                  Expanded(

                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15, right: 15,),
                                      child: Column(

                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Book Name:${items[index]['book Name']}",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                overflow: TextOverflow.ellipsis),
                                          ),
                                          Text(
                                            "Author Name:${items[index]['author Name']}",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                overflow: TextOverflow.ellipsis),
                                          ),
                                          Text(
                                            "Department:${items[index]['department']}",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.normal,
                                                overflow: TextOverflow.ellipsis),
                                          ),
                                          Text(
                                            "Semester:${items[index]['semester']}",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.normal),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "৳:${items[index]['price']}",
                                                style: TextStyle(fontSize: 20),
                                              ),
                                              MaterialButton(
                                                onPressed: () {
                                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailsPages(ptoductDetails: items[index])));
                                                },
                                                shape: StadiumBorder(),
                                                color: Colors.blueAccent,
                                                child: Text("Buy now"),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (context, index) {
                          return SizedBox(
                            height: 10,
                          );
                        },
                        itemCount: items.length),
              ),

            ],
          )),
    );
  }

  getData(String keyWord) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    var data = await firestore.collection(keyWord).get();
    for (var doc in data.docs) {
      setState(() {
        Map map = {
          "book Name": doc["book Name"],
          "author Name": doc["author Name"],
          "department": doc["department"],
          "semester": doc["semester"],
          "price": doc["price"],
        };
        items.add(map);
      });
    }
  }
  void addToCard({required int index}) {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    firestore.collection('user_cart').add({
      'book Name':items[index]['book Name'],
      'author Name':items[index]['author Name'],
      'department':items[index]['department'],
      'semester':items[index]['semester'],
      'image':items[index]['image'],
      'price':items[index]['price'],
     });
  }
  getDataBySearch(String collectionName)async{
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    var data = await firestore.collection(collectionName).where('title',isEqualTo:searchController.text).get();
    for (var doc in data.docs) {
      setState(() {
        Map map={
          "book Name": doc["book Name"],
          "author Name": doc["author Name"],
          "department": doc["department"],
          "semester": doc["semester"],
          "price": doc["price"],
        };
        items.add(map);
      });
      print(items);
    }
  }
}
