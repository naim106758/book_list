
import 'package:book_list/Pages/cart_pages.dart';
import 'package:book_list/Pages/home_pages.dart';
import 'package:book_list/Pages/profile_pages.dart';
import 'package:book_list/Pages/wishlist_pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StorePages extends StatefulWidget {
  const StorePages({ Key? key, String? user }) : super(key: key);

  @override
  _StorePagesState createState() => _StorePagesState();
}

class _StorePagesState extends State<StorePages> {
  List pages=[ const HomePages(),const WishlistPages(),const CartPages(),const ProfilePages()];
  int index=0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:pages[index],
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: index,
          onTap: (value){
            index=value;
            setState(() {

            });
          },
          type: BottomNavigationBarType.fixed,

          items:const [
            BottomNavigationBarItem(icon: Icon(Icons.home),label: 'Home Page'),
            BottomNavigationBarItem(icon: Icon(Icons.favorite),label: 'Wishlist'),
            BottomNavigationBarItem(icon: Icon(Icons.shopping_cart),label: 'Cart'),
            BottomNavigationBarItem(icon: Icon(Icons.person),label: 'Profile')
          ]),

    );
  }
}