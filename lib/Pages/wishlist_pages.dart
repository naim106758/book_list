import 'package:book_list/Details/details_pages.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../route/route_name.dart';

class WishlistPages extends StatefulWidget {
  const WishlistPages({ Key? key }) : super(key: key);

  @override
  _WishlistPagesState createState() => _WishlistPagesState();
}

class _WishlistPagesState extends State<WishlistPages> {
  List items=[];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
getWishlistData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        appBar: AppBar(
          backgroundColor: Colors.blue.shade400,
          leading: BackButton( onPressed: (){
            Navigator.pushNamed(context, RouteName.StorePages);
          },),
          title:Text('Your WishList has ${items.length} item(s)',style: const TextStyle(fontSize: 18),),
        ),
        body:SafeArea(
          child: Container(
              padding:const EdgeInsets.only(left:6,right:6),
              child:items.isEmpty?const Center(child: CircularProgressIndicator(
                color: Colors.greenAccent,
              ),):
              Column(
                children: [
                  const SizedBox(height: 10,),
                  Expanded(

                    child: items.isEmpty
                        ? Center(
                      child: CircularProgressIndicator(),
                    )
                        :
                    ListView.separated(
                        itemBuilder: (context, index) {
                          return Container(
                            height: 150,
                            decoration: BoxDecoration(
                                color: Colors.greenAccent.shade200,
                                borderRadius: BorderRadius.circular(20)),
                            child: Row(
                              children: [
                                Expanded(

                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      left: 15, right: 15,),
                                    child: Column(

                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Book Name:${items[index]['book Name']}",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              overflow: TextOverflow.ellipsis),
                                        ),
                                        Text(
                                          "Author Name:${items[index]['author Name']}",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              overflow: TextOverflow.ellipsis),
                                        ),
                                        Text(
                                          "Department:${items[index]['department']}",
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.normal,
                                              overflow: TextOverflow.ellipsis),
                                        ),

                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Semester:${items[index]['semester']}",
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.normal),
                                            ),
                                            Text(
                                              "৳:${items[index]['price']}",
                                              style: TextStyle(fontSize: 20),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: [

                                            MaterialButton(
                                              onPressed: () {
                                                removeItemFromWishlist(index);
                                               },
                                              shape: StadiumBorder(),
                                              color: Colors.blueAccent,
                                              child: Text("Remove"),
                                            ),
                                            MaterialButton(
                                              onPressed: () {
                                                Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailsPages(ptoductDetails: items[index])));
                                               },
                                              shape: StadiumBorder(),
                                              color: Colors.blueAccent,
                                              child: Text("Buy now"),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                        separatorBuilder: (context, index) {
                          return SizedBox(
                            height: 10,
                          );
                        },
                        itemCount: items.length),
                  )

                ],
              )
          ),
        )

    );
  }
  getWishlistData()async{
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    var wishlistData=await firestore.collection('wish_list').get();
    for (var doc in wishlistData.docs) {setState(() {
      Map map = {
        "book Name": doc["book Name"],
        "author Name": doc["author Name"],
        "department": doc["department"],
        "semester": doc["semester"],
        "price": doc["price"],
        'id':doc.id,
       };
      items.add(map);
    });
    }
  }
  void removeItemFromWishlist(int index) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    await firestore.collection('wish_list').doc(items[index]['id']).delete();
    items.clear();
    getWishlistData();
    setState(() {});
  }
}
