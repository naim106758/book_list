import 'package:book_list/Buttom/buy_button.dart';
import 'package:flutter/material.dart';


class PeyMentPages extends StatefulWidget {
  const PeyMentPages({Key? key}) : super(key: key);

  @override
  State<PeyMentPages> createState() => _PeyMentPagesState();
}

class _PeyMentPagesState extends State<PeyMentPages> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              BuyButtom(
                text: 'Bkash PeyMent',
                onPressed: () {},
              ),
              BuyButtom(
                text: 'Nagad PeyMent',
                onPressed: () {},
              ),
              BuyButtom(
                text: 'Rocket PeyMent',
                onPressed: () {},
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  MaterialButton(
                      color: Colors.indigo,
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context);
                      }

                  ),
                  MaterialButton(
                      color: Colors.indigo,

                      child: Text('Ok'),
                      onPressed: () {}

                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}


