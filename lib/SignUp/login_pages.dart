
import 'package:book_list/SignUp/user.dart';
import 'package:book_list/route/route_name.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../Buttom/custom_buttom.dart';
import '../Pages/store_pages.dart';

class LoginPages extends StatefulWidget {
  const LoginPages({Key? key}) : super(key: key);

  @override
  State<LoginPages> createState() => _LoginPagesState();
}

class _LoginPagesState extends State<LoginPages> {
  TextEditingController mailController = TextEditingController();
  TextEditingController passwerdController = TextEditingController();
  bool isVisible = true;
  FirebaseAuth? firebaseAuth;
  bool? showSpinner;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseAuth = FirebaseAuth?.instance;
    showSpinner = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.all(10),
        color: Colors.white70,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/Book 2.jpg',
              height:100,
              width:200,
            ),
            const Text(
              "Login",
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.indigoAccent),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              onChanged: (value) {
                setState(() {});
              },
              controller: mailController,
              decoration: InputDecoration(
                  hintText: "Enter your mail",
                  labelText: "Email",
                  prefixIcon: const Icon(Icons.mail),
                  suffixIcon: mailController.text.isEmpty
                      ? const Text(" ")
                      : GestureDetector(
                          onTap: () {
                            mailController.clear();
                          },
                          child: const Icon(Icons.close),
                        ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              controller: passwerdController,
              obscureText: isVisible,
              decoration: InputDecoration(
                hintText: "Enter password",
                labelText: "password",
                prefixIcon: const Icon(Icons.lock),
                suffixIcon: GestureDetector(
                  onTap: () {
                    isVisible = !isVisible;
                    setState(() {});
                  },
                  child: const Icon(Icons.visibility),
                ),
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8))),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            CustomButtom(
              text: 'Login',
              onPressed: () {
                userSignIn();
              }),
            const SizedBox(
              height: 15,
            ),
            Row(
              //crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Dont have an account?",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal),
                ),
                TextButton(
                    onPressed: () {
                       Navigator.pushNamed(context, RouteName.RegisterPages);
                    },
                    child: const Text(
                      "Create one",
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.normal),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }
  userSignIn() async {
    if (mailController.text.isEmpty) {
      Alert(
          context: context,
          type: AlertType.error,
          title: "mail",
          desc: "Please Enter your mail address")
          .show();
    } else if (passwerdController.text.length < 5) {
      Alert(
          context: context,
          type: AlertType.error,
          title: "mail",
          desc: "Please Enter your passwerd")
          .show();
    }
    else {
      setState(() {
        showSpinner = true;
      });
      try {
        UserCredential userCredential = await FirebaseAuth.instance
            .signInWithEmailAndPassword(
            email: mailController.text, password: passwerdController.text);

        var user = userCredential.user;
        if (user!.email == mailController.text) {
          user_id = user.uid;
          email_id = mailController.text;
           Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>StorePages()), (route) => false);
        } else {
          setState(() {
            showSpinner = false;
          });
          Alert(
              context: context,
              type: AlertType.error,
              title: "Failed LogIn",
              desc: " ")
              .show();
        }
      }
      catch (error) {
        setState(() {
          showSpinner = false;
        });
        Alert(
            context: context,
            type: AlertType.error,
            title: "Failed LogIn",
            desc: error.toString())
            .show();
      }

    }
  }
}
