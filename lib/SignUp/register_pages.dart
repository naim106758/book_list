import 'package:book_list/SignUp/login_pages.dart';
import 'package:book_list/route/route_name.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../Buttom/custom_buttom.dart';

class RegisterPages extends StatefulWidget {
  const RegisterPages({Key? key}) : super(key: key);

  @override
  State<RegisterPages> createState() => _RegisterPagesState();
}

class _RegisterPagesState extends State<RegisterPages> {
  TextEditingController fnameController = TextEditingController();
  TextEditingController lnameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController mailController = TextEditingController();
  TextEditingController passwerdController = TextEditingController();
  bool isVisible = true;
  FirebaseAuth? firebaseAuth;
  bool? showSpinner;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseAuth = FirebaseAuth?.instance;
    showSpinner = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.all(10),
        color: Colors.white70,
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/Book 1.jpg',
                  height:100,
                  width:400,
                ),
                const Text(
                  "Registation",
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Colors.indigoAccent),
                ),
                const SizedBox(
                  height: 15,
                ),
                TextField(
                  onChanged: (value) {
                    setState(() {});
                  },
                  controller: fnameController,
                  decoration: InputDecoration(
                      hintText: "Enter your Fast Name",
                      labelText: "Fast Name",
                      prefixIcon: const Icon(Icons.person),
                      suffixIcon: fnameController.text.isEmpty
                          ? const Text(" ")
                          : GestureDetector(
                        onTap: () {
                          fnameController.clear();
                        },
                        child: const Icon(Icons.close),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                const SizedBox(
                  height:7,
                ),
                TextField(
                  onChanged: (value) {
                    setState(() {});
                  },
                  controller: lnameController,
                  decoration: InputDecoration(
                      hintText: "Enter your Last Name",
                      labelText: "Last Name",
                      prefixIcon: const Icon(Icons.person),
                      suffixIcon: lnameController.text.isEmpty
                          ? const Text(" ")
                          : GestureDetector(
                        onTap: () {
                          lnameController.clear();
                        },
                        child: const Icon(Icons.close),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                const SizedBox(
                  height:7,
                ),
                TextField(
                  onChanged: (value) {
                    setState(() {});
                  },
                  controller: phoneController,
                  decoration: InputDecoration(
                      hintText: "Enter your Phone Number",
                      labelText: "Number",
                      prefixIcon: const Icon(Icons.phone),
                      suffixIcon: phoneController.text.isEmpty
                          ? const Text(" ")
                          : GestureDetector(
                        onTap: () {
                          phoneController.clear();
                        },
                        child: const Icon(Icons.close),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                const SizedBox(
                  height:7,
                ),
                TextField(
                  onChanged: (value) {
                    setState(() {});
                  },
                  controller: mailController,
                  decoration: InputDecoration(
                      hintText: "Enter your mail",
                      labelText: "Email",
                      prefixIcon: const Icon(Icons.mail),
                      suffixIcon: mailController.text.isEmpty
                          ? const Text(" ")
                          : GestureDetector(
                        onTap: () {
                          mailController.clear();
                        },
                        child: const Icon(Icons.close),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                const SizedBox(
                  height:7,
                ),
                TextField(
                  controller: passwerdController,
                  obscureText: isVisible,
                  decoration: InputDecoration(
                    hintText: "Enter password",
                    labelText: "password",
                    prefixIcon: const Icon(Icons.lock),
                    suffixIcon: GestureDetector(
                      onTap: () {
                        isVisible = !isVisible;
                        setState(() {});
                      },
                      child: const Icon(Icons.visibility),
                    ),
                    border: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                  ),
                ),
                const SizedBox(
                  height:7,
                ),
                CustomButtom(
                  text: 'Registetion',
                  onPressed: ()   {
                    UserSingUp();
                    saveUserDara();
                  },
                ),
                const SizedBox(
                  height:7,
                ),
                Row(
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      "Dont have an account?",
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal),
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.pushNamed(context, RouteName.LoginPages);
                        },
                        child: const Text(
                          "Sign In",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.normal),
                        ))
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
  void UserSingUp() async {
    if (mailController.text.isEmpty) {
      Alert(
          context: context,
          type: AlertType.error,
          title: "mail",
          desc: "Please Enter your mail address")
          .show();
    } else if (passwerdController.text.length < 5) {
      Alert(
          context: context,
          type: AlertType.error,
          title: "password",
          desc: "Please Enter your password address")
          .show();
    }else {
      setState(() {
        showSpinner = true;
      });

      if (fnameController.text.isNotEmpty &&
          mailController.text.isNotEmpty &&
          phoneController.text.isNotEmpty &&
          passwerdController.text.isNotEmpty) {
        try {
          UserCredential userCredential =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
            email: mailController.text,
            password: passwerdController.text,
          );
          if (userCredential.user!.email == mailController.text) {
            Navigator.pushNamed(context, RouteName.LoginPages);
          }
        } catch (e) {
          setState(() {
            showSpinner = false;
          });
        }
      } else {
        setState(() {
          showSpinner = false;
        });
        Alert(
            context: context,
            type: AlertType.error,
            title: "Please fill all field",
            desc: " ")
            .show();
      }
    }
  }

  saveUserDara() {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    firestore.collection("user").doc(mailController.text).set({
      "name": fnameController.text,
      "mail": mailController.text,
      "phone": phoneController.text,
      "password": passwerdController.text,
    });
  }
}
