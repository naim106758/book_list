import 'package:book_list/route/route_name.dart';
import 'package:book_list/route/routes.dart';
import 'package:book_list/splash_screens.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Book List',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      // home:SplashScreens(),
      initialRoute: RouteName.SplashScreens,
      onGenerateRoute:Routes.generateRoute,
    );
  }
}

