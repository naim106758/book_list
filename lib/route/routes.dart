
import 'package:book_list/Pages/store_pages.dart';
import 'package:book_list/SignUp/login_pages.dart';
import 'package:book_list/SignUp/register_pages.dart';
import 'package:book_list/route/route_name.dart';
import 'package:book_list/splash_screens.dart';
import 'package:flutter/material.dart';

import '../PeyMent/Pey_ment_pages.dart';
class Routes{
  static  Route<dynamic> generateRoute(RouteSettings settings){

    switch (settings.name){
      case RouteName.SplashScreens:{
        return MaterialPageRoute(builder: (context)=>SplashScreens());
      }
      case RouteName.LoginPages:{
        return MaterialPageRoute(builder: (context)=>LoginPages());
      }
      case RouteName.StorePages:{
        return MaterialPageRoute(builder: (context)=>StorePages());
      }
      case RouteName.RegisterPages:{
        return MaterialPageRoute(builder: (context)=>RegisterPages());
      }
      case RouteName.PeyMentPages:{
        return MaterialPageRoute(builder: (context)=>PeyMentPages());
      }
      default:
        return MaterialPageRoute(builder: (context){
          return Scaffold(
              body: Center(child: Text("No route defined"),)
          );
        });

    }
  }
}
